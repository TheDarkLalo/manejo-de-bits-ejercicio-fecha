import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner tec = new Scanner(System.in);
        System.out.println("Ingrese el dia: ");
        int dia = tec.nextInt();
        Date.getInstance().setDia(dia);
        System.out.println("Ingrese el mes: ");
        int mes = tec.nextInt();
        Date.getInstance().setMes(mes);
        System.out.println("Ingrese el año: ");
        int año = tec.nextInt();
        Date.getInstance().setAño(año);
        System.out.println("Ingrese la hora: ");
        int hora = tec.nextInt();
        Date.getInstance().setHora(hora);
        System.out.println("Ingrese los minutos: ");
        int minuto = tec.nextInt();
        Date.getInstance().setMinuto(minuto);
        System.out.println(Date.getInstance().toString());
    }
}

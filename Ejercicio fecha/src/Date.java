public class Date {

    /*
    * data --> 32 bits     * [0, 11] --> 12 bits --> año
    * [12, 15] --> 4 bits --> mes
    * [16, 20] --> 5 bits --> dia
    * [21, 25] --> 5 bits --> hora
    * [26, 31] --> 6 bits --> minuto
    */

    private int data;

    private static Date instance = null;

    public static Date getInstance() {
        if (instance == null) {
            instance = new Date();
        }
        return instance;
    }

    public Date() {
        data = 0;
    }

    public int getAño() {
        int año = data >>> 20;
        return año;
    }

    public int getMes() {
        int mes = (data << 12) >>> 28;
        return mes;
    }

    public int getDia() {
        int dia = (data << 16) >>> 27;
        return dia;
    }

    public int getHora() {
        int hora = (data << 21) >>> 27;
        return hora;
    }

    public int getMinuto() {
        int minuto = (data << 26) >>> 26;
        return minuto;
    }

    public void setAño(int año) {
        if (año >= 0 && año < 4096) {
            int mask = 1048575; //00000000000011111111111111111111
            data = (data & mask) | (año << 20);
            System.out.println("DATA: " + Long.toBinaryString(data));
        } else{
            System.out.println("Año no ingresado");
        }
    }

    public void setMes(int mes) {
        if (mes > 0 && mes < 13) {
            int mask = 2146500607 | (1 << 31); //01111111111100001111111111111111
            data = (data & mask) | (mes << 16);
            System.out.println("DATA: " + Long.toBinaryString(data));
        } else {
            System.out.println("Mes no ingresado");
        }
    }

    public void setDia(int dia) {
        if (dia > 0 && dia < 32) {
            int mask = 2147420159 | (1 << 31); //01111111111111110000011111111111
            data = (data & mask) | (dia << 11);
            System.out.println("DATA: " + Long.toBinaryString(data));
        } else {
            System.out.println("Dia no ingresado");
        }
    }

    public void setHora(int hora) {
        if (hora >= 0 && hora < 24) {
            int mask = 2147481663 | (1 << 31); //01111111111111111111100000111111
            data = (data & mask) | (hora << 6);
            System.out.println("DATA: " + Long.toBinaryString(data));
        } else {
            System.out.println("Hora no ingresada");
        }
    }

    public void setMinuto(int minuto) {
        if (minuto >= 0 && minuto < 60) {
            int mask = 2147483584 | (1 << 31); //01111111111111111111111111000000
            data = (data & mask) | (minuto);
            System.out.println("DATA: " + Long.toBinaryString(data));
        } else {
            System.out.println("Minuto no ingresado");
        }
    }

    public boolean sameDate(Date t) {

        return false;
    }

    public boolean isBefore(Date t) {
        return false;
    }

    public boolean isAfter(Date t) {
        return false;
    }

    @Override
    public String toString() {
        return getDia() + "/" + getMes() + "/" + getAño() + " " + getHora() + ":" + getMinuto();
    }
}